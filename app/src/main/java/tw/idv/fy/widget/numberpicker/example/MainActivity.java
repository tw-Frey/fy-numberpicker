package tw.idv.fy.widget.numberpicker.example;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import java.util.Locale;

import tw.idv.fy.widget.numberpicker.NumberPicker;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        NumberPicker numberPicker = findViewById(R.id.number_picker);
        numberPicker.setOnValueChangedListener((NumberPicker.OnValueChangeListener)
                (picker, oldVal, newVal) -> android.util.Log.v("Faty",
                        String.format(Locale.TAIWAN, "%.1f -> %.1f = %.1f (%d)",
                                oldVal, newVal, picker.getValueF(), picker.getValue()
                        )
                )
        );
    }
}
