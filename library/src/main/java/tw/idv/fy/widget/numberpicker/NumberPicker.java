package tw.idv.fy.widget.numberpicker;

import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.widget.TextViewCompat;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.common.base.Preconditions;
import com.google.common.collect.FluentIterable;
import com.google.common.primitives.Floats;

import java.util.List;

import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

@SuppressWarnings("unused")
public class NumberPicker extends android.widget.NumberPicker implements android.widget.NumberPicker.OnValueChangeListener {

    private static final List<Float> PICKER_VALUES_LIST = Floats.asList(
            24.0f, 23.5f, 23.0f, 22.5f, 22.0f, 21.5f, 21.0f,
            20.5f, 20.0f, 19.5f, 19.0f, 18.5f, 18.0f, 17.5f,
            17.0f, 16.5f, 16.0f, 15.5f, 15.0f, 14.5f, 14.0f,
            13.5f, 13.0f, 12.5f, 12.0f, 11.5f, 11.0f, 10.5f,
            10.0f,  9.5f,  9.0f,  8.5f,  8.0f,  7.5f,  7.0f,
             6.5f,  6.0f,  5.5f,  5.0f,  4.5f,  4.0f,  3.5f,
             3.0f,  2.5f,  2.0f,  1.5f,  1.0f,  0.5f,  0.0f
    );
    private static final int MAX_VALUE = PICKER_VALUES_LIST.size() - 1;
    private static final int MIN_VALUE = 0;

    @SuppressWarnings("Guava")
    private static final String[] DISPLAYED_VALUES_ARRAY = FluentIterable.from(PICKER_VALUES_LIST)
                                                                         .transform(Object::toString)
                                                                         .toArray(String.class);

    private static final LinearLayout.LayoutParams BUTTON_LAYOUT_PARAMS = new LayoutParams(WRAP_CONTENT, WRAP_CONTENT);

    @IdRes
    private static final int NUMBERPICKER_INPUT;
    @IdRes
    private static final int DECREMENT;
    @IdRes
    private static final int INCREMENT;
    static {
        Resources RES = Resources.getSystem();
        NUMBERPICKER_INPUT = RES.getIdentifier("numberpicker_input", "id", "android");
        DECREMENT          = RES.getIdentifier("decrement"         , "id", "android");
        INCREMENT          = RES.getIdentifier("increment"         , "id", "android");
    }

    private OnValueChangeListener mValueChangedListener;
    private final View mNumberText;

    public NumberPicker(Context context) {
        this(context, null);
    }

    public NumberPicker(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public NumberPicker(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        this(context, attrs, defStyleAttr);
    }

    public NumberPicker(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, 0);
        super.setDisplayedValues(DISPLAYED_VALUES_ARRAY); // must be invoke this first
        super.setOrientation(HORIZONTAL);
        ResetTextView(mNumberText = findViewById(NUMBERPICKER_INPUT));
        ResetButton(findViewById(DECREMENT), true);
        ResetButton(findViewById(INCREMENT));
        initial();
    }

    @Override
    public final void onValueChange(android.widget.NumberPicker picker, int oldVal, int newVal) {
        if (mNumberText != null) mNumberText.clearFocus();
        if (mValueChangedListener == null || oldVal == newVal) return;
        mValueChangedListener.onValueChange(this, getValueF(oldVal), getValueF(newVal));
    }

    /**
     * 請使用另一個 {@link NumberPicker#setOnValueChangedListener(tw.idv.fy.widget.numberpicker.NumberPicker.OnValueChangeListener)}
     */
    @Override
    public final void setOnValueChangedListener(android.widget.NumberPicker.OnValueChangeListener onValueChangedListener) {
        throw new UnsupportedOperationException("請使用另一個 setOnValueChangedListener ");
    }

    @Override
    public final void setDisplayedValues(String[] displayedValues) {
        throw new UnsupportedOperationException("禁用");
    }

    @Override
    public final void setOrientation(int orientation) {
        throw new UnsupportedOperationException("禁用");
    }

    @Override
    public final void setMinValue(int minValue) {
        throw new UnsupportedOperationException("禁用");
    }

    @Override
    public void setMaxValue(int maxValue) {
        throw new UnsupportedOperationException("禁用");
    }

    public void setValueF(float valueF) {
        Preconditions.checkArgument(
                PICKER_VALUES_LIST.contains(valueF),
                "要 0 ~ 24 之間的浮點數, 且小數點非 .0 即 .5"
        );
        super.setValue(PICKER_VALUES_LIST.indexOf(valueF));
    }

    public float getValueF() {
        return getValueF(super.getValue());
    }

    private float getValueF(int index) {
        return PICKER_VALUES_LIST.get(index);
    }

    public void setOnValueChangedListener(@Nullable OnValueChangeListener onValueChangedListener) {
        mValueChangedListener = onValueChangedListener;
    }

    private void initial() {
        super.setOnValueChangedListener(this);
        super.setMinValue(MIN_VALUE);
        super.setMaxValue(MAX_VALUE);
        setWrapSelectorWheel(false);
        setValueF(0);
    }

    private static void ResetButton(@Nullable View v) {
        ResetButton(v, false);
    }

    private static void ResetButton(@Nullable View v, boolean decrement) {
        if (v == null) return;
        if (decrement) v.setScaleX(-1);
        v.setPadding(0, 0, 0, 0);
        v.setLayoutParams(BUTTON_LAYOUT_PARAMS);
        v.setBackgroundResource(R.drawable.content_activity_game_controller_btn_picker);
    }

    private static void ResetTextView(@Nullable View v) {
        if (!(v instanceof TextView)) return;
        ViewGroup.LayoutParams lp = v.getLayoutParams();
        lp.width = v.getResources().getDimensionPixelSize(R.dimen.tw_idv_fy_widget_numberpicker_txt_width);
        lp.height = v.getResources().getDimensionPixelSize(R.dimen.tw_idv_fy_widget_numberpicker_txt_height);
        v.setLayoutParams(lp);
        v.setBackground(null);
        v.setFocusable(false);
        v.setFocusableInTouchMode(false);
        TextViewCompat.setTextAppearance(
                (TextView) v,
                R.style.tw_idv_fy_widget_numberpicker_TextAppearance
        );
        ((TextView) v).setInputType(InputType.TYPE_NULL);
    }

    public interface OnValueChangeListener {
        void onValueChange(NumberPicker picker, float oldVal, float newVal);
    }
}